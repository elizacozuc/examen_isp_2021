import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ex2 extends JFrame {
    JTextArea tArea;
    JButton button;

    ex2() {
        setTitle("Ex2");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        init();
        setSize(500, 300);
        setVisible(true);
    }

    void init() {
        this.setLayout(null);

        tArea = new JTextArea();
        tArea.setBounds(100, 80, 250, 100);

        button = new JButton("Apasa");
        button.setBounds(100, 200, 80, 40);
        button.addActionListener(new TratareButon());
        add(button);
        add(tArea);
    }

    public static void main(String[] args) {
        new ex2();
    }


    private class TratareButon implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            File file = new File("src/main/java/data");
            try {
                tArea.getText();
                FileWriter fr = new FileWriter(file, true);
                BufferedReader bf = new BufferedReader(fr);
                String a;
                a = bf.readLine();
                while (a != null) {
                    fr.write(a);
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
